# README #

Self-hosted Web Application for hosting and serving own library of MP3s

### What is this repository for? ###

* Provides a Laravel based system for uploading and content managing a library of MP3 from CD and Album backups,
* effectively replicating basic (re:very basic) spotify functionality

### Technical

* Built on Laravel 5.5 using Vuejs for UI

### How do I get set up? ###

* https://laravel.com/docs/5.5#server-requirements

### Who do I talk to? ###

* Richard Gaunt