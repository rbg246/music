<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create roles

        $roles = [
          [
              'name' => 'admin',
              'display_name' => 'Administrator',
              'description' => 'Able to set application, add users',
          ],
          [
              'name' => 'uploader',
              'display_name' => 'Uploader',
              'description' => 'Upload and maintain music collection',
          ],
          [
              'name' => 'listener',
              'display_name' => 'Listener',
              'description' => 'Able to listen to music',
          ]
        ];

        foreach ($roles as $role) {
            $r = new Role();
            $r->fill($role);
            $r->save();
        }
    }
}
