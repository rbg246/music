<?php

//use App\Models\User;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
          [
            'name' => 'richard',
            'email' => 'richard@bournegaunt.com',
            'password' => bcrypt(env('MOCK_PASSWORD', NULL)),
            'roles' => ['admin']
          ],
          [
            'name' => 'listener',
            'email' => 'listener@bournegaunt.com',
            'password' => bcrypt(env('MOCK_PASSWORD', NULL)),
            'roles' => ['listener']
          ],
          [
            'name' => 'uploader',
            'email' => 'uploader@bournegaunt.com',
            'password' => bcrypt(env('MOCK_PASSWORD', NULL)),
            'roles' => ['uploader']
          ],
          [
            'name' => 'admin',
            'email' => 'admin@bournegaunt.com',
            'password' => bcrypt(env('MOCK_PASSWORD', NULL)),
            'roles' => ['admin']
          ]
        ];

        // adding users
        foreach ($users as $user) {
            $insert = array_filter($user, function ($key) {
                return ($key !== 'roles');
            }, ARRAY_FILTER_USE_KEY);
            DB::table('users')->insert($insert);
        }

        // adding roles to users
        foreach ($users as $data) {
            /** @var \App\Models\User $user */
            $user = User::where('email', $data['email'])->first();
            foreach ($data['roles'] as $role_name) {
                $role = Role::where('name', '=', $role_name)->first();
                $user->attachRole($role);
            }
            $user->save();
        }

    }
}
