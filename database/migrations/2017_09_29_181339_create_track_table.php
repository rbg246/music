<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 512)->default('Default Title');
            $table->integer('album_id')->unsigned();
            $table->integer('artist_id')->unsigned();
            $table->foreign('album_id')
                ->references('id')
                ->default(1)
                ->on('albums');
            $table->foreign('artist_id')
                ->references('id')
                ->default(1)
                ->on('artists');
            $table->text('file_path');
            $table->integer('length')->unsigned()->nullable();
            $table->smallInteger('track_number')->unsigned()->nullable();
            $table->boolean('processed')
                ->default(TRUE);
            $table->string('genre', 512)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracks');
        Schema::dropIfExists('albums');
        Schema::dropIfExists('artists');
    }
}
