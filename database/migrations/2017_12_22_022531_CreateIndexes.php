<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //adding search indexes for track, album and artist tables

        Schema::table('tracks', function (Blueprint $table) {
                $table->index('title');
        });

        Schema::table('artists', function (Blueprint $table) {
            $table->index('name');
        });

        Schema::table('albums', function (Blueprint $table) {
            $table->index('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
