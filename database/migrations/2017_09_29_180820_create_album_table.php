<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('albums', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 512)->default('Default AlbumResource');
            $table->integer('artist_id')->unsigned();
            $table->integer('year')
                ->unsigned()->nullable();
            $table->foreign('artist_id')
                ->default(1)
                ->references('id')
                ->on('artists');
            $table->unique(['title', 'artist_id']);
            $table->timestamps();
        });


        // create default album
        \App\Models\Album::create([
            'title' => 'Unknown AlbumResource',
            'artist_id' => 1
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracks');
        Schema::dropIfExists('albums');
        Schema::dropIfExists('artists');
    }
}
