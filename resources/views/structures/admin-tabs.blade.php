<ul class="m-admin-actions nav nav-tabs">
    <li class="nav-item {{ (Route::currentRouteName() === 'upload') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('upload' )}}">Track upload</a>
    </li>
    <li class="nav-item {{ (Route::currentRouteName() === 'unprocessedTracks') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('unprocessedTracks') }}">Unprocessed tracks</a>
    </li>
    <li class="nav-item {{ (Route::currentRouteName() === 'editTracks') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('editTracks') }}">Track edit</a>
    </li>
    <li class="nav-item {{ (Route::currentRouteName() === 'editAlbums') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('editAlbums') }}">Album edit</a>
    </li>
    @role('admin')
    <li class="nav-item {{ (strpos(Route::currentRouteName(), 'user.') !== FALSE) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('user.index') }}">User Admin</a>
    </li>
    @endrole
</ul>
