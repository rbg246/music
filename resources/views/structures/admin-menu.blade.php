<ul class="nav navbar-nav navbar-right">
    <!-- Authentication Links -->
    @auth
        <li>
            <a href="">
                {{ Auth::user()->name }}
            </a>
        </li>
        @ability('admin,uploader', '')
        <li class="dropdown">
            <a class="dropdown-toggle" id="nav-dropdown" data-toggle="dropdown" aria-haspopup="true"
               aria-expanded="true">
                <i class="glyphicon glyphicon-cog"></i>
                <i class="glyphicon glyphicon-chevron-down"></i>
            </a>
            <ul class="dropdown-menu" aria-labelledby="nav-dropdown">
                <li><a href="{{ route('upload') }}">Upload</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{ route('unprocessedTracks') }}">Unprocessed tracks</a></li>
                <li><a href="{{ route('editTracks') }}">Edit tracks</a></li>
                <li><a href="{{ route('editAlbums') }}">Edit albums</a></li>
                <li><a href="{{ route('editArtists') }}">Edit artists</a></li>
                <li role="separator" class="divider"></li>
                @role('admin')
                <li><a href="{{ route('user.index') }}">User administration</a></li>
                <li role="separator" class="divider"></li>
                @endrole
                <li>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
        @endability
        @endguest
</ul>