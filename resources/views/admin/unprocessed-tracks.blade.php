@extends('layouts.admin')

@section('content')
@if (sizeof($tracks)  > 0)
    <form method="post" action="{{ Request::url() }}">
    {{ csrf_field() }}
    <table class="table">
        <thead>
            @foreach ($headers as $heading)
                <th>{{$heading}}</th>
            @endforeach
        </thead>
        <tbody>
        @foreach ($tracks as $track)
            <tr>
                <td>
                    <input type="text" name="artist_name_{{$track->id}}" placeholder="{{ $track->artist }}"/>
                </td>
                <td>
                    <input type="text" name="track_title_{{$track->id}}" placeholder="{{ $track->track }}" />
                </td>
                <td>
                    <input type="text" name="album_title_{{$track->id}}" placeholder="{{ $track->album }}"/>
                </td>
                <td>
                    <input type="text" name="track_number_{{$track->id }}" placeholder="{{ $track->track_number }}" />
                </td>

                <td>
                    <a href="/listen/{{$track->id}}">listen</a>
                    <input type="hidden" name="old_value_{{$track->id}}" value="{{ json_encode($track) }}" />
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <button class="btn btn-success" type="submit">Process</button>
</form>
@else
    <h3>No Unprocessed Tracks</h3>
@endif
@endsection