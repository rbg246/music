@extends('layouts.admin')

@section('content')
    @if (count($rows) > 0)
        <div class="m-albums-edit">
            @include('structures.table', ['headers' => $headers, 'rows' => $rows])
        </div>
    @else
        <h3>No albums found</h3>
    @endif
@endsection