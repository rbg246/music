@extends('layouts.admin')

@section('content')
    <div class="container">
        <h2>Artist Edit</h2>
        <form method="post" action="{{ Request::url() }}">
            {{csrf_field()}}

            <div class="form-group">
                <label for="album">Artist name</label>
                <input id="autocomplete-artist" class="form-control" name="name" type="text" placeholder="{{ $artist->name }}"/>
            </div>

            <div class="form-group">
                <input type="submit" value="delete" />
                <input type="submit" value="update" class="btn btn-success"/>
            </div>
        </form>
    </div>
@endsection