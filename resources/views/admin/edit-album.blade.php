@extends('layouts.admin')

@section('content')
    <div class="container">
    <h2>Album Edit</h2>
    <form method="post" action="{{ Request::url() }}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="title">Album title</label>
            <input class="form-control" name="title" type="text" placeholder="{{ $album->title }}"/>
        </div>
        <div class="formgroup">
            <label for="artist">Artist name</label>
            <input id="autocomplete-artist" class="form-control" name="artist" type="text" placeholder="{{ $album->artist->name }}"/>
        </div>

        <div class="m-album-thumbnail form-group">
            <label for="album_cover">Album artwork</label>
            <div style="clear:left; margin: 15px 0;">
                <img class="img-responsive" src="{{$album->get_artwork_url()}}" />
            </div>
            <input class="form-control" name="album_cover" type="file" />

        </div>
        <div class="form-group">
            <input value="Save" type="submit" class="btn btn-success"/>
        </div>
    </form>
</div>
@endsection