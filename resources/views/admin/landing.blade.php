@extends('layouts.admin')

@section('content')

    <h1>{{ $title }}</h1>

    @if (count($rows) > 0)
        @include('structures.table', ['headers' => $headers, 'rows' => $rows])
    @els $
        <h3>No {{ $title }} found</h3>
    @endif

@endsection