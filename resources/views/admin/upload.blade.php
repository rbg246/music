@extends('layouts.admin')
@section('content')
    <form id="trackUploader" class="dropzone" method="POST" action="{{ Request::url() }}">
        {{ csrf_field() }}
@endsection('content')

@section('scripts')
    <script src="/js/dropzone.min.js"></script>
    <script>
        Dropzone.options.trackUploader = {
          'paramName' : 'track',
          'acceptedFiles' : '.mp3',
        };
    </script>
@endsection
