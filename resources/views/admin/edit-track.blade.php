@extends('layouts.admin')

@section('content')
    <div class="container">
        <h2>Track Edit</h2>
        <form method="post" action="{{ Request::url() }}">
            {{csrf_field()}}
            <div class="form_group">
                <label for="title">Track title</label>
                <input class="form-control" name="title" type="text" placeholder="{{ $track->title }}" />
            </div>
            <div class="form-group">
                <label for="album">Album title</label>
                <input id="autocomplete-album" class="form-control" name="album" type="text" placeholder="{{ $track->album->title }}"/>
            </div>
            <div class="formgroup">
                <label for="artist">Artist name</label>
                <input id="autocomplete-artist" class="form-control" name="artist" type="text" placeholder="{{ $track->artist->name }}"/>
            </div>

            <div class="form-group">
                <label for="track">Track</label>
                <div class="container" style="margin: 15px 0;">
                    <audio controls src="{{ $track->get_track_url() }}"></audio>
                </div>
                <input class="form-control" name="track" type="file" />
            </div>
            <div class="form-group">
                <input type="submit" value="Delete" class="btn btn-warning" />
                <input type="submit" value="Update" class="btn btn-default"/>
            </div>
        </form>
    </div>
@endsection