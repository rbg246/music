@extends('layouts.admin')

@section('content')

    @if (count($rows) > 0)
        @include('structures.table', ['headers' => $headers, 'rows' => $rows])
    @else
        <h3>No tracks uploaded yet</h3>
    @endif

@endsection