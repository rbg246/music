@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <p>
            <a class="btn btn-success" href="/admin/user/create">Create New User</a>
        </p>
        @if (sizeof($rows)  > 0)
            @include('structures.table', ['headers' => $headers, 'rows' => $rows])
        @else
            <p>No Users</p>
        @endif
    </div>
@endsection