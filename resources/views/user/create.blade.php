@extends('layouts.admin')

@section('content')
    <div class="container">
        <h2>Create User</h2>
        @include ('user.form', [
            'available_roles' => $available_roles,
            'action' => '/admin/user',
            'method' => 'post',
            'button_label' => 'Create'
        ])
    </div>
@endsection('content')