<form method="POST" action="{{ $action }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field($method) }}
    <div class="row">
        <div class="form-group col-lg-8">
            <label for="name">Name</label>
            <input class="form-control" type="text" name="name" value="{{ isset($user['name'])
                ? $user['name'] : ''}}">
        </div>
        <div class="form-group col-lg-8">
            <label for="email">Email</label>
            <input class="form-control" type="text" name="email" value="{{ isset($user['email'])
                ? $user['email'] : ''}}">
        </div>
        <div class="form-group col-lg-8">
            <label for="password">Password</label>
            <input class="form-control" type="password" name="password" value="">
        </div>
        <div class="form-group col-lg-8">
            <label for="password">Confirm Password</label>
            <input class="form-control" type="password" name="password_confirmation" value="">
        </div>
    </div>
    <div>
        <h4>User Roles</h4>
        @if (count($available_roles) > 0)
            <fieldset>
                @foreach ($available_roles as $role)
                    <div class="form-group">
                        <label>
                            <input type="checkbox" name="roles[]" value="{{ $role['id'] }}"
                                    {{ isset($user_roles)
                                    && in_array($role, $user_roles) ? ' checked=checked' : '' }}
                            />
                            {{ $role['display_name'] }}
                        </label>
                    </div>
                @endforeach
            </fieldset>
        @else
            <p>No roles found</p>
        @endif
        <div class="form-group">
            <button class="btn btn-large btn-success" type="submit">{{ $button_label }}</button>
        </div>
    </div>
</form>