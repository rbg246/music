@extends('layouts.admin')

@section('content')
    <div class="container">
            <h2>Edit User - {{ $user['name'] }}</h2>
            @include ('user.form', [
                'user' => $user,
                'user_roles' => $user_roles,
                'available_roles' => $available_roles,
                'action' => '/admin/user/' . $user['id'],
                'method' => 'put',
                'button_label' => 'Update'
            ])
            @include ('user.delete', [
                'user' => $user
            ])
    </div>
@endsection('content')