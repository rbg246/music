<h3>Dangerous actions</h3>
<form method="post" action="/admin/user/{{ $user['id'] }}">
    {{ csrf_field() }}
    {{ method_field('delete') }}
    <div class="alert alert-warning" role="alert">Warning - Users can not be recovered once deleted</div>
    <button class="btn btn-danger" type="submit">Delete user</button>
</form>