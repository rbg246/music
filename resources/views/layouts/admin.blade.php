<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    @include ('structures.admin-styles')
    @yield ('admin.header.styles')
</head>
<body>
    <div>
        @include ('structures.admin-header')
        @include ('structures.messages')

        <div class="container-fluid">
            @yield('content')
        </div>
    </div>

    @include('structures.footer')
</body>
</html>
