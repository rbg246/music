/**
 * Vue Router
 * setup of routes
 */

const routes = [
  {
    name : 'home',
    path : '/',
    component : components.albumsList
  },
  {
    name : 'album',
    path : '/album/:id',
    component : components.albumList
  },
  {
    name: 'artist',
    path : '/artist/:id',
    component : components.artistList
  },
  {
    name : 'playlistEdit',
    path : '/playlist/:id/edit',
    component : components.PlaylistEdit
  },
  {
    name : 'playlist',
    path : '/playlist/:id',
    component: components.Playlist
  }
];

export const router = new VueRouter({
  routes: routes,
  mode : 'history',
  base : '/app/'
});

