
/**
 * app setup
 * bootstrap includes dependencies, libraries and plugins
 * components vue component registration
 * routes vue router and route controller registration
 */

require('./bootstrap');
require('./filters');
require('./components');
require('./routes');

const app = new Vue(require('./components/App.vue'));
const search = new Vue(require('./components/Search.vue'));