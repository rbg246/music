
import {EventBus} from "../event-bus";
import {Events} from "../events";

let isUndefined = require('lodash.isundefined');

/**
 * sends event for notifying user
 * @param message
 * @param type
 */
let setMessage = (message, type) => {
  type = (!isUndefined(type)) ? type : 'success';
  EventBus.$emit(Events.notifyUser, {
    message : message,
    type  : type
  });
};

export {setMessage};