/**
 * registering vue components
 * @type {{}}
 */


// base components
Vue.component('sidebar', require('./components/SideBar.vue'));
Vue.component('controls', require('./components/AudioPlayer.vue'));
Vue.component('player', require('./components/Player.vue'));
Vue.component('progressBar', require('./components/ProgressBar.vue'));
Vue.component('album-tile', require('./components/AlbumTile.vue'));
Vue.component('album-header', require('./components/AlbumHeader.vue'));
Vue.component('track-row', require('./components/TrackRow.vue'));
Vue.component('track-table', require('./components/TrackTable.vue'));
Vue.component('playlist-row', require('./components/PlayListRow.vue'));
Vue.component('messages', require('./components/Messages.vue'));

// router view components

window.components = {};
components.albumsList =
    Vue.component('albums-list', require('./components/AlbumsList.vue'));
components.albumList =
    Vue.component('album-list', require('./components/AlbumList.vue'));

components.Playlist =
    Vue.component('play-list', require('./components/Playlist.vue'));

components.PlaylistEdit =
    Vue.component('play-list-edit', require('./components/PlaylistEdit.vue'));

components.artistList =
    Vue.component('artist-list', require('./components/ArtistList.vue'));
