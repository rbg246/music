

let helpers = {};

/**
 * builds path for a given type
 * @param id
 * @param file_type
 * @returns null | string
 */
helpers.getPath = function (id, file_type) {
  let filePath = null;
  switch (file_type) {
    case 'coverArt' :
      filePath = '/files/artwork/' + id + '.jpg';
      break;

    case 'trackUrl' :
      filePath = '/files/tracks/' + id + '.mp3';
      break;

    case 'albumLink' :
      filePath = '/album/' + id;
      break;

    case 'artistLink' :
      filePath = '/artist/' + id;
      break;

    case 'trackLink' :
      filePath = '/track/' + id;
      break;
  }
  return filePath;
};

helpers.fileTypes = {
  coverArt : 'coverArt',
  trackUrl : 'trackUrl',
  albumLink : 'albumLink',
  artistLink : 'artistLink',
  trackLink : 'trackLink'
};

export default helpers;