/**
 * API endpoints for client side app
 * @type {{}}
 */
let Endpoint = {
  playlist : {
    index : 'playlist',
    get : function (id) { return 'playlist/' + id;},
    create : 'playlist',
    delete : function (id) { return 'playlist/' + id;},
    update : function (id) {return 'playlist/' + id;}
  },
  playlistItem : {
    create : function (playlistId) {
      return 'playlist/' + playlistId + '/item';
    },
    delete : function (playlistId, itemId) {
      return 'playlist/' + playlistId + '/item/' + itemId;
    }
  },
  user : {
    get : 'user',
    update : 'user'
  }
};

export {Endpoint};