/**
 * commonly used filters
 */
/**
 * formats time to N hrs  N hr N s
 */
Vue.filter('formatSecondsLong', function (seconds) {
  const HOUR = 3600;
  const MINUTE = 60;
  const SECOND = 1;

  let hours = 0;
  let minutes = 0;
  let ret = '';

  // round seconds to integer
  seconds = Math.floor(seconds);

  // set the hours
  if (seconds > HOUR) {
    hours = parseInt(Math.floor(seconds / HOUR));
    seconds -= (HOUR * hours);
    let label = (hours > 1) ? 'hrs ' : 'hr ';
    ret += hours + label;
  }

  // set the minutes
  if (seconds > MINUTE) {
    minutes = Math.floor(seconds / MINUTE);
    seconds -= (MINUTE * minutes);
    let label = (minutes > 1) ? 'mins ' : 'min ';
    ret += minutes + label;
  }

  // set the remaining seconds
  if (seconds > 0) {
    let label = 's ';
    ret += seconds + label;
  }

  return ret;

});

Vue.filter('formatSecondsShort', function (seconds) {

  const MINUTE = 60;
  const SECOND = 1;

  let minutes = 0;
  let ret = '';

  // round seconds to integer
  seconds = Math.floor(seconds);

  // set the minutes
  if (seconds > MINUTE) {
    minutes = Math.floor(seconds / MINUTE);
    seconds -= (MINUTE * minutes);
    minutes = (minutes < 10) ? '0' + minutes : minutes;
  } else {
    minutes = '0';
  }
  ret += minutes + ':';

  // set the remaining seconds
  seconds = (seconds < 10) ? '0' + seconds : seconds;
  ret += seconds;

  return ret;

});