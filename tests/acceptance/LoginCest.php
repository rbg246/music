<?php

class LoginCest
{
    public function _before(AcceptanceTester $I)
    {
      $dotenv = new Dotenv\Dotenv(__DIR__ . '/../../');
      $dotenv->load();
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function loginToSystem(AcceptanceTester $I)
    {

      $username = env('MOCK_USERNAME', '');
      $password = env('MOCK_PASSWORD', '');
      $I->login($username, $password);
      $I->see('richard');
    }
}
