<?php


class APICest
{
    public function _before(AcceptanceTester $I)
    {
      $dotenv = new Dotenv\Dotenv(__DIR__ . '/../../');
      $dotenv->load();
      $username = env('MOCK_USERNAME', '');
      $password = env('MOCK_PASSWORD', '');
      $I->login($username, $password);
    }

    public function _after(AcceptanceTester $I)
    {
    }

    /**
     * test to see if api returns the default artist
     * @param AcceptanceTester $I
    **/
    public function getArtist(AcceptanceTester $I)
    {
      $I->sendGET('artist/1');
      $I->seeResponseContainsJson([
        'id' => 1,
        'artist_name' => 'Default Artist',
        'albums' => [
            0 => [
              'id' => 1,
              'title' => 'Unknown Album'
            ]
        ]
      ]);
    }

  /**
   * test the default album
   * @param \AcceptanceTester $I
   */
    public function get_album (AcceptanceTester $I) {
      $I->sendGET('album/1');
      $I->seeResponseContainsJson([
        'id' => 1,
        'album_title' => 'Unknown Album',
        'artist_name' => 'Default Artist',
        'artist_id' => 1
      ]);
    }

  /**
   * get the first track
   * @param \AcceptanceTester $I
   */
    public function get_track(AcceptanceTester $I) {
      $I->sendGET('track/1');
      $I->seeResponseContainsJson([
        'id' => 1
      ]);
    }

    public function get_paginated_artists(AcceptanceTester $I) {
      $I->sendGET('albums/1');
      $I->seeResponseContainsJson([
        'albums' => [
          0 => [
            'id' => 1
          ]
        ]
      ]);
    }


  /**
   * get paginated albums
   * @param \AcceptanceTester $I
   */
    public function get_paginated_albums(AcceptanceTester $I) {
      $I->sendGET('artists/1');
      $I->seeResponseContainsJson([
        'artists' => [
          0 => [
            'id' => 1
          ]
        ]
      ]);
    }

  /**
   * get paginated artists
   * @param \AcceptanceTester $I
   */
    public function get_paginated_tracks(AcceptanceTester $I) {
      $I->sendGET('tracks/1');
      $I->seeResponseContainsJson([
        'tracks' => [
          0 => [
            'id' => 1
          ]
        ]
      ]);
    }
}
