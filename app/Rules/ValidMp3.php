<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use getID3;

class ValidMp3 implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $file)
    {
        //$file = Input::file('upload'); // assumed name of the file input is upload
        $path = $path = $file->getRealPath();
        $id3 = new getID3();
        $tags = $id3->analyze($path);
        if(is_array($tags) && array_key_exists('audio', $tags)) {
            // valid
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The file must be mp3';
    }
}
