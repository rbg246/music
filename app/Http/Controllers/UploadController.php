<?php

namespace App\Http\Controllers;


use App\Models\Track;
use App\Rules\ValidMp3;
use Illuminate\Http\Request;

class UploadController extends Controller
{

    const INPUT_ARTIST_NAME = 'artist_name';
    const INPUT_TRACK_NAME = 'track_name';
    const INPUT_ALBUM_NAME = 'album_name';
    const INPUT_FILE = 'track';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tracks = Track::all();
        return view('admin.upload', [
            'tracks' => $tracks
        ]);
    }

    /**
     * @param Request $request
     */
    public function upload(Request $request) {
        $request->validate([
          self::INPUT_FILE => ['required', new ValidMp3]
        ]);
        $file = $request->file(self::INPUT_FILE);
        $attributes = [
        'file' => $file,
        ];
        $track = Track::create($attributes);
        if ($track) {
            $request->session()->flash('status', 'TrackResource added . ' . $track->name);
        } else {
            $request->session()->flash('status', 'Error in adding track');
        }
       return redirect('admin/upload');
    }


}
