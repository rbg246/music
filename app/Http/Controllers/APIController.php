<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Artist;
use App\Models\Playlist;
use App\Models\Track;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class APIController extends Controller
{
    public $chunk_size = 50;
    public function __construct() {
      $this->middleware('auth');
    }

  /**
   * updates users queue
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\JsonResponse
   */
    public function update_user(Request $request) {
      $queue = $request->input('user');
      $user = Auth::user();

      if ($queue !== NULL
          && is_array($queue)
          &&  isset($queue['currentTrack'])
          && isset($queue['playQueue'])) {
        $user->play_queue = json_encode($queue);
        $user->save();
        return response()->json([
          'user' => $queue
        ]);
      } else {
        return response()->json([
          'error' => TRUE,
          'message' => 'queue was not valid JSON or NULL',
        ], 400);
      }
    }

  /**
   * get artist and albums
   * @param \App\Models\Artist $artist
   *
   * @return \Illuminate\Http\JsonResponse
   */
    public function get_artist (Artist $artist)
    {
      if ($artist !== NULL) {
        $artist_albums = Album::where(['artist_id' => $artist->id])
          ->select(['id','title', 'artist_id', 'year', 'album_cover'])
          ->get();
        $albums = [];
        foreach ($artist_albums as $album) {
          $albums[] = $this->_get_album($album);
        }
        $ret = [
          'id' => $artist->id,
          'artist_name' => $artist->name,
          'albums' => $albums
        ];

        return response()->json($ret);
      }

      return response()->json([
        'error' => TRUE,
        'message' => 'ArtistResource not found'
      ], 404);
    }

  /**
   * get album and associated tracks
   * @param \App\Models\Artist $album
   *
   * @return \Illuminate\Http\JsonResponse
   */
    public function get_album (Album $album)
    {
      if ($album !== NULL) {
        $ret = $this->_get_album($album);
        return response()->json($ret);
      }
      return response()->json([
        'error' => TRUE,
        'message' => 'AlbumResource not found'
      ]);
    }

  /**
   * helper function getting an album and tracks
   * @param \App\Models\Album $album
   *
   * @return array
   */
    private function _get_album(Album $album) {

      $tracks = Track::where([
        'album_id' => $album->id,
        'processed' => 1
      ])
        ->with('artist')
        ->with('album')
        ->orderBy('track_number', 'ASC')
        ->get();

      $tracks = $this->parse_tracks($tracks);

      $ret = [
        'id' => $album->id,
        'album_cover' => '/files/artwork/' . $album->id . '.jpg',
        'album_title' => $album->title,
        'album_link' => '/album/' . $album->id,
        'album_year' => $album->year,
        'artist_name' => $album->artist->name,
        'artist_id' => $album->artist->id,
        'artist_link' => 'artists/' . $album->artist->id,
        'tracks' => $tracks
      ];

      return $ret;
    }

  /**
   * adds artist, album info to each track for track tables
   * @param $tracks
   * @return array $ret
   */
    private function parse_tracks ($tracks) {
      $ret = [];
      // attaching artist to each track
      foreach ($tracks as &$track) {
        $ret[] = [
          'id' => $track->id,
          'title' => $track->title,
          'track_length' => $track->length / 1000,
          'track_number' => $track->track_number,
          'albumId' => $track->album->id,
          'albumCover' => '/files/artwork/' . $track->album->id . '.jpg',
          'albumLink' => '/album/' . $track->album->id,
          'albumTitle' => $track->album->title,
          'artistId' => $track->artist->id,
          'artistName' => $track->artist->name,
          'artistLink' => '/artist/' . $track->artist->id,
          'src' => '/files/tracks/' . $track['id'] . '.mp3'
        ];
      }
      return $ret;
    }

  /**
   * gets a track and associated album and artist
   * @param \App\Models\Track $track
   *
   * @return \Illuminate\Http\JsonResponse
   */
    public function get_track (Track $track)
    {
      if ($track !== NULL) {
        $ret = [
          'id' => $track->id,
          'title' => $track->title,
          'track_length' => $track->length,
          'track_number' => $track->number,
          'artist_id' => $track->artist_id,
          'artist_name' => $track->artist->name,
          'album_id' => $track->album_id,
          'album_title' => $track->album->title,
          'album_year' => $track->album->year,
        ];
        return response()->json($ret);
      }
      return response()->json([
        'error' => TRUE,
        'message' => 'No track found'
      ], 404);
    }

  /**
   * gets page of artists pagination
   * @param $page
   *
   * @return \Illuminate\Http\JsonResponse
   */
    public function get_artists ($page) {
      $artists = Artist::where('id', '!=', 1)->simplePaginate($this->chunk_size, ['id', 'name'] , '', $page);
      $collection = [];
      if (count($artists) > 0) {
        foreach ($artists as $artist) {
          $collection[] = [
            'id' => $artist->id,
            'name' => $artist->name
          ];
        }
        $ret = [
          'artists' => $collection
        ];
        return response()->json($ret);
      }
      return response()->json([
        'error' => TRUE,
        'message' => 'Page not found'
      ], 404);
    }

  /**
   * gets page of albums
   * @param $page
   *
   * @return \Illuminate\Http\JsonResponse
   */
    public function get_albums ($page) {
      $albums = Album::where('id', '!=', '1')->simplePaginate($this->chunk_size, ['*'] , '', $page);
      $collection = [];
      if (count($albums) > 0) {
        $ret = [];
        foreach ($albums as $album) {
          $collection[] = [
            'id' => $album->id,
            'title' => $album->title,
            'artist_id' => $album->artist_id,
            'artist_name' => $album->artist->name,
            'year' => $album->year
          ];
          $ret = [
            'albums' => $collection
          ];
        }
        return response()->json($ret);
      }
      return response()->json([
        'error' => TRUE,
        'message' => 'Page not found'
      ], 404);
    }

  /**
   * returns a page of tracks
   * @param $page
   *
   * @return \Illuminate\Http\JsonResponse
   */
    public function get_tracks ($page) {
      $tracks = Track::simplePaginate($this->chunk_size, ['*'] , '', $page);
      $collection = [];
      if (count($tracks) > 0) {
        $ret = [];
        foreach ($tracks as $track) {
          $collection[] = [
            'id' => $track->id,
            'name' => $track->title,
            'album_id' => $track->album_id,
            'album_name' => $track->album->title,
            'artist_id' => $track->artist_id,
            'artist_name' => $track->artist->name
          ];
          $ret = [
            'tracks' => $collection
          ];
        }
        return response()->json($ret);
      }
      return response()->json([
        'error' => TRUE,
        'message' => 'Page not found'
      ], 404);

    }

    public function get_albums_by_artist (Artist $artist) {
      if ($artist !== NULL) {
        $albums = Album::where('artist_id', '=', $artist->id)
          ->select('id', 'title', 'year', 'album_cover')
          ->get()
          ->toArray();
        $ret = [
          'albums' => $albums
        ];
        return response()->json($ret);
      }
      return response()->json([
        'error' => TRUE,
        'message' => 'Page not found'
      ], 404);
    }

  /**
   * get tracks by artist
   * @param \App\Models\Artist $artist
   *
   * @return \Illuminate\Http\JsonResponse
   */
    public function get_tracks_by_artist (Artist $artist) {
      if ($artist !== NULL) {
        $tracks = Track::where('artist_id', '=', $artist->id)
          ->select('id', 'name', 'album_id')
          ->get()
          ->toArray();
        $ret = [
          'tracks' => $tracks
        ];
        return response()->json($ret);
      }
      return response()->json([
        'error' => TRUE,
        'message' => 'Page not found'
      ], 404);
    }

  /**
   * get tracks by album
   * @param \App\Models\Album $album
   *
   * @return \Illuminate\Http\JsonResponse
   */
    public function get_tracks_by_album(Album $album) {
      if ($album !== NULL) {
        $tracks = Track::where('album_id', '=', $album->id)
          ->select('id', 'name', 'artist_id')
          ->get()
          ->toArray();
        $ret = [
          'tracks' => $tracks
        ];
        return response()->json($ret);
      }
      return response()->json([
        'error' => TRUE,
        'message' => 'Page not found'
      ], 404);
    }

  /**
   * adds track to either new playlist or existing
   * @param \Illuminate\Http\Request $request
   * @param \App\Models\Playlist|NULL $playlist
   *
   * @return \Illuminate\Http\JsonResponse
   */
    public function add_track_to_playlist(Request $request, Playlist $playlist = NULL) {
      $user = Auth::user();
      $track_id = $request->input('trackId');
      if ($playlist === null) {
        $playlist_title = $request->input('title');
        $playlist = Playlist::create([
          'title' => $playlist_title,
          'user_id' => $user->id
        ]);
        $playlist->save();
      }
      $ret = $playlist->add_track($track_id);
      return response()->json($ret);
    }

  /**
   * returns  a playlist
   * @param \App\Models\Playlist|NULL $playlist
   *
   * @return \Illuminate\Http\JsonResponse
   */
    public function get_playlist (Playlist $playlist = null) {
      $track_ids = $playlist->tracks->map(function($track) {
        return $track->track_id;
      })->toArray();
      $tracks = Track::whereIn('id', $track_ids)
        ->where('processed', '=', 1)
        ->with('artist', 'album')
        ->orderBy('track_number', 'id')
        ->get();
      $tracks = $this->parse_tracks($tracks);
      $ret = [
        'playlist' => $playlist,
        'tracks' => $tracks
      ];
      return response()->json($ret);
    }

  /**
   * returns index of all playlists
   * @return \Illuminate\Http\JsonResponse
   */
    public function get_playlists() {
      $user = Auth::user();
      $playlists = Playlist::where('user_id', $user->id)
        ->get();
      return response()->json([
        'playlists' => $playlists
      ]);
    }


}
