<?php

namespace App\Http\Controllers;

use App\Models\Playlist\PlaylistItem;
use Illuminate\Http\Request;

class PlaylistItemController extends Controller
{

    protected $rules = [
      'trackId' => 'required|integer'
    ];
    /**
     * @param \Illuminate\Http\Request
     * @param int $playlist_id
     *
     * Store newly created resource
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $playlist_id)
    {
        $request->validate($this->rules);

        $attrs = [
            'track_id' => $request->input('trackId'),
            'playlist_id' => $playlist_id
        ];

        $item = new PlaylistItem();
        $item->fill($attrs);
        $item->save();
        return response()->json($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $playlist_id, $track_id)
    {
        $item = PlaylistItem::where('playlist_id', '=', $playlist_id)
          ->where('track_id', '=', $track_id)
          ->first();
        $item->delete();
        return response()->json($item);
    }

}
