<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Artist;
use App\Models\Track;
use Illuminate\Http\Request;

class MusicController extends Controller
{
    public function index () {
      return redirect()->route('upload');
    }

  /**
   * handles view of all tracks
   */
  public function tracks () {

    $rows = Track::all_tracks();

    $headers = [
      'Id',
      'ArtistResource',
      'AlbumResource',
      'TrackResource #',
      'TrackResource',
      'Length'
    ];



    return view('admin.tracks', [
      'headers' => $headers,
      'rows' => $rows
    ]);

  }

  /**
   * app view
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function home () {
    return view('home', []);
  }


  /**
   * handles view of all albums
   */
  public function albums () {
    $albums = Album::all_albums();
    $rows = [];
    /**
     * @var Album $album
     */
    foreach ($albums as $album) {
      $rows[] = [
        $album->artist_name,
        $album->album_title,
        '<a href="' . $album->get_album_view_url() .'">view</a>'
      ];
    }
    $headers = [
      'ArtistResource',
      'AlbumResource',
      'view'
    ];
    return view('albums', [
      'headers' => $headers,
      'rows' => $rows
    ]);
  }

  /**
   * handles view of all artists
   */
  public function unprocessed_tracks () {

    $tracks = Track::unprocessed_tracks();
    return view('admin.unprocessed-tracks', [
      'tracks' => $tracks,
      'headers' => ['ArtistResource', 'TrackResource', 'AlbumResource', 'TrackResource #', '' ]
    ]);

  }

  /**
   * processes unprocessed tracks
   * @param \Illuminate\Http\Request $request
   */
  public function process_tracks (Request $request) {
    $needles = [
      'artist_name_',
      'track_title_',
      'album_title_',
      'track_number_'
    ];
    $inputs = $request->input();
    $track_updates = [];
    foreach($inputs as $input_key => $input) {
      if ($input !== NULL) {
        foreach ($needles as $needle) {
          $id = (str_contains($input_key, $needle)) ? str_replace($needle, '', $input_key) : NULL;
          if ($id !== NULL && (int) $id !== 0) {
            $track_updates[$id][substr($needle, 0, sizeof($needle) - 2)] = $input;
          }
        }
      }
    }
    Track::process_unprocessed_tracks($track_updates);
    redirect('unprocessedTracks')->with('status', 'Tracks processed');
  }

  public function editAlbumList () {

    $albums = Album::where('id', '!=', '1')
      ->get();

    $rows = [];

    /**
     * @var \App\Models\Album $album
     */
    foreach ($albums as $album) {

      $rows[] = [
        '<img src="' . $album->get_artwork_url() . '" />',
        $album->artist->name,
        $album->title,
        '<a class="btn btn-success" href="' . $album->get_album_edit_url() .'">edit</a>'
      ];
    }

    $headers = [
      '',
      'ArtistResource',
      'AlbumResource',
      ''
    ];
    return view('admin.albums', [
      'headers' => $headers,
      'rows' => $rows,
    ]);
  }

  /**
   * creates form for edit album
   * @param \App\Models\Album $album
   *
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   * @internal param \Illuminate\Http\Request $request
   */
  public function editAlbum(Album $album) {
    $js = [
      'autocomplete' => [
        'artists' => Artist::get()->pluck('name')
      ]
    ];
    \JavaScript::put($js);
    return view('admin.edit-album', ['album' => $album]);

  }

  /**
   * submit handler for edit album
   * @param \Illuminate\Http\Request $request
   * @param \App\Models\Album $album
   *
   * @return \Illuminate\Http\RedirectResponse
   */
  public function editAlbumSubmit (Request $request, Album $album) {
    $title = $request->input('title');
    $artist_name = $request->input('artist');
    $updated = FALSE;

    if ($title !== NULL) {
      $album->title = $title;
      $updated = TRUE;
    }

    if ($artist_name !== NULL) {
      $artist = Artist::firstOrCreate([
        'name' => $artist_name
      ]);
      $album->artist_id = $artist->id;
      $updated = TRUE;
    }

    if ($request->hasFile('album_cover')) {
      $file = $request->file('album_cover');
      $album->store_artwork($file);
      $updated = TRUE;
    }
    if ($updated) {
      $album->save();
    }
    // fix redirect
    return redirect('/admin/albums/edit')->with('status', $album->title . ' - updated');
  }

  /**
   * creates list of tracks with edit link
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function editTrackList () {
    $tracks = Track::where('id', '!=', '1')
      ->orderBy('artist_id', 'album_id', 'track_number')
      ->get();

    $rows = [];

    /**
     * @var \App\Models\Album $album
     */
    foreach ($tracks as $track) {

      $rows[] = [
        $track->artist->name,
        $track->album->title,
        $track->track_number,
        $track->title,
        '<a class="btn btn-default" href="' . $track->get_edit_url() .'">edit</a>'
      ];
    }

    $headers = [
      'ArtistResource',
      'AlbumResource',
      '#',
      'TrackResource',
      'edit'
    ];
    return view('admin.tracks', [
      'headers' => $headers,
      'rows' => $rows,
    ]);
  }

  /**
   * creates form for editing tracks
   * @param \App\Models\Track $track
   */
  public function editTrack (Track $track) {
    $js = [
      'data' => [
        'autocomplete' => [
          'artist' => Artist::where('id', '!=', '1')->get()->pluck('name'),
          'album' => Album::where('id', '!=', '1')->get()->pluck('title')
        ]
      ]
    ];
    \JavaScript::put($js);

    return view('admin.edit-track', ['track' => $track]);

  }

  public function editTrackSubmit(Request $request, Track $track) {

    // fields to update
    $artist = $request->input('artist');
    $title = $request->input('title');
    $album = $request->input('album');
    $has_file = $request->hasFile('track');

    $updated = FALSE;

    if ($title !== NULL && $track->title !== $title) {
      $track->title = $title;
      $updated = TRUE;
    }

    if ($artist !== NULL && $track->artist->name !== $artist) {
      $new_artist = Artist::firstOrCreate(['name' => $artist]);
      $track->artist_id = $new_artist->id;
      $updated = TRUE;
    }


    if ($album !== NULL && $track->album->name !== $album) {
      $new_album = Album::firstOrCreate([
        'title' => $album,
        'artist_id' => $track->artist->id
      ]);
      $track->album_id = $new_album->id;
      $updated = TRUE;
    }

    if ($updated) {
      $track->save();
    }

    return redirect('admin/tracks/track/' . $track->id . '/edit')->with('status', $track->title . ' - updated');

  }

  public function artists () {
    $artists = Artist::where('id', '!=', '1')
      ->get();

    $rows = [];

    /**
     * @var \App\Models\Album $artist
     */
    foreach ($artists as $artist) {

      $rows[] = [
        $artist->name,
      ];
    }

    $headers = [
      'ArtistResource'
    ];

    return view('admin.landing', [
      'title' => 'Artists',
      'headers' => $headers,
      'rows' => $rows,
    ]);
  }

  public function editArtistList (Artist $artist) {
    $headers = [
      'ArtistResource',
      ''
    ];

    $artists = Artist::where('id', '!=', '1')
      ->get();
    $rows = [];
    foreach ($artists as $artist) {
      $rows[] = [
        $artist->name,
        '<a href="' . $artist->get_artist_edit_url() .'" class="btn btn-default">edit</a>'
      ];
    }

    return view('admin.landing', [
      'title' => 'Artists',
      'headers' => $headers,
      'rows' => $rows,
    ]);
  }

  public function editArtist (Artist $artist) {
    $js = [
      'data' => [
        'autocomplete' => [
          'artist' => Artist::where('id', '!=', '1')->get()->pluck('name'),
        ]
      ]
    ];
    \JavaScript::put($js);

    return view('admin.edit-artist', ['artist' => $artist]);
  }

  /**
   * submit handling for edit artist
   * @param \App\Models\Artist $artist
   * @param \Illuminate\Http\Request $request
   */
  public function editArtistSubmit(Artist $artist, Request $request) {
    $name = $request->input('name');
    $updated = FALSE;
    if ($name !== $artist->name) {
      $artist->name = $name;
      $updated = TRUE;
    }

    if ($updated) {
      $artist->save();
    }

    redirect('editArtists')->with('status', $artist->name . ' updated');

  }
  public function __construct() {
    $this->middleware('auth');
  }
}
