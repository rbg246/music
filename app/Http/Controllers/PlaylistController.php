<?php

namespace App\Http\Controllers;

use App\Http\Resources\PlaylistResource;
use App\Models\Playlist;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PlaylistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $playlists = Playlist::with('tracks')
            ->get();
        return PlaylistResource::collection($playlists);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return PlaylistResource
     */
    public function store(Request $request)
    {
        $rules = [
          'title' => 'required'
        ];

        $request->validate($rules);
        $playlist = new Playlist();
        $playlist->fill([
            'title' => $request->input('title'),
            'user_id' => Auth::user()->id
        ]);
        $playlist->save();

        return new PlaylistResource($playlist);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return PlaylistResource
     */
    public function show($id)
    {
        $playlist = Playlist::find($id);
        return new PlaylistResource($playlist);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return PlaylistResource | \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
          'title' => 'required'
        ];

        $request->validate($rules);

        $playlist = Playlist::find($id);
        $playlist->title = $request->input('title');
        $playlist->save();
        return new PlaylistResource($playlist);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $playlist_id
     * @return Exception | boolean
     */
    public function destroy($playlist_id)
    {
        // delete playlist items
        DB::table('playlist_items')
          ->where('playlist_id', $playlist_id)
          ->delete();

        $playlist = Playlist::destroy($playlist_id);

        return ($playlist);

    }
}
