<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Track;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{

  /**
   * stream a music track to client
   *
   * @param \App\Http\Controllers\Track|\App\ModelsTrack $track
   *
   * @return \Symfony\Component\HttpFoundation\StreamedResponse
   */

  public function stream_track (Track $track) {
    $file_path = $track->file_path;
    return $this->stream($file_path);
  }

  /**
   * stream file artwork to client
   * @param \App\Models\Album $album
   *
   * @return \Symfony\Component\HttpFoundation\StreamedResponse
   */
  public function serve_artwork (Album $album) {
    $file_path = $album->album_cover;
    $exists = Storage::exists($file_path);
    if ($exists) {
      return $this->stream($file_path);
    } else {
      return redirect('/images/music.png');
    }
  }

  /**
   * create a stream for a given path
   * @param string $file_path
   *
   * @return \Symfony\Component\HttpFoundation\StreamedResponse
   */
  public function stream($file_path) {
    $length = Storage::size($file_path);
    return response()->stream(function() use ($file_path) {
      $stream = Storage::readStream($file_path);
      fpassthru($stream);
      if (is_resource($stream)) {
        fclose($stream);
      }
    }, 200, [
      'Cache-Control'         => 'must-revalidate, post-check=0, pre-check=0',
      'Content-Type'          => Storage::mimeType($file_path),
      'Content-Length'        => Storage::size($file_path),
      'Content-Disposition'   => 'attachment; filename="' . basename($file_path) . '"',
      'Pragma'                => 'public',
      'Accept-Ranges'         => 'bytes',
      'Content-Range'         => 'bytes 0-' . $length
    ]);
  }

  public function __construct() {
    $this->middleware('auth');
  }
}
