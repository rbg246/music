<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreate;
use App\Http\Requests\UserUpdate;
use App\Http\Resources\UserResource;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UserController extends Controller
{

    /**
     * deletes a user
     * @param $primaryKey
     * @param null $parentKey
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($primaryKey, $parentKey = null)
    {
        $user = User::find($primaryKey);
        $user_name = $user->name;
        $user->delete();
        return redirect()
          ->route('user.index')
          ->with('status', $user_name . ' has been deleted');
    }

    /**
     * stores a user
     * @param \App\Http\Requests\UserCreate $request
     * @param null $parentKey
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserCreate $request, $parentKey = null)
    {

        $fillable = ['name', 'password', 'email'];
        $attrs = array_intersect_key($request->input(), array_flip($fillable));
        $user = new User();
        $user->fill($attrs);
        $user->save();
        $roles = $request->input('roles');
        $user->attachRoles($roles);
        return redirect()
          ->route('user.create')
          ->with('status', 'User created - ' . $user->name);
    }

    /**
     * returns edit user form
     * @param $primaryKey
     *
     * @return \Illuminate\View\View
     */
    public function edit ($primaryKey) : View {
        $user = User::with('roles')->find($primaryKey);
        $user = $user->toArray();

        foreach ($user['roles'] as $ind => $role) {
            unset($role['pivot']);
            $user['roles'][$ind] = $role;
        }
        $roles = $user['roles'];
        unset($user['roles']);
        $available_roles = Role::all()->toArray();
        return view('user.edit', [
          'user' => $user,
          'user_roles' => $roles,
          'available_roles' => $available_roles
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int|string $primaryKey
     * @param null $parentKey
     *
     * @return Resource
     */
    public function update(UserUpdate $request, $primaryKey)
    {

        $user = User::find($primaryKey);

        // update roles
        $updated_roles = $request->input('roles');
        $user->detachRoles($user->roles);
        $user->attachRoles($updated_roles);


        // update props
        $user->name = $request->input('name');
        $user->email = $request->input('email');

        // update password if new value
        $pass = $request->input('password');
        if ($pass !== '' && isset($pass) ) {
            $user->password = $request->input('password');
        }

        $user->save();

        return redirect()
          ->route('user.edit', ['id' => $user['id']])
          ->with('status', 'User updated');

    }

    /**
     * return table of all users
     * @param null $parentKey
     *
     * @return \Illuminate\View\View
     */
    public function index($parentKey = null) : View
    {
        $users = User::all()->toArray();
        $keys = ['id', 'name', 'email'];
        $rows = [];
        foreach ($users as $ind => $user) {
            $row = array_intersect_key($user, array_flip($keys));
            $row['edit'] = '<a href="/admin/user/'. $user['id']. '/edit" class="btn btn-success">Edit</a>';
            $rows[] = $row;
        }
        $headers = ['name', 'email', ''];
        return view('user.index', ['headers' => $headers, 'rows' => $rows]);
    }


    public function show($primaryKey)
    {
        return redirect()
          ->route('user.edit', ['id' => $primaryKey]);
    }

    /**
     * returns a view to create a user
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        $available_roles = Role::all()->toArray();
        return view('user.create', [
          'available_roles' => $available_roles
        ]);
    }
}
