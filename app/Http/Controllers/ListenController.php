<?php

namespace App\Http\Controllers;
use App\Models\Track;
use Illuminate\Support\Facades\Storage;

class ListenController extends Controller
{
    /**
     * streams the s3 file to the user
     * @param Track $track
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function serve(Track $track) {
        $file_path = $track->file_path;
        return response()->stream(function() use ($track, $file_path) {
            $stream = Storage::readStream($file_path);
            fpassthru($stream);
            if (is_resource($stream)) {
                fclose($stream);
            }
        }, 200, [
            'Cache-Control'         => 'must-revalidate, post-check=0, pre-check=0',
            'Content-Type'          => Storage::mimeType($file_path),
            'Content-Length'        => Storage::size($file_path),
            'Content-Disposition'   => 'attachment; filename="' . basename($file_path) . '"',
            'Pragma'                => 'public',
        ]);

    }

    /**
     * @param Track $track
     */
    public function listen (Track $track) {
        return View('listen', ['track' => $track]);
    }

    public function __construct() {
//      $this->middleware('auth');
    }
}
