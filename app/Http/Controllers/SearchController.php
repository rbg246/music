<?php

namespace App\Http\Controllers;

use App\Http\Resources\SearchResource;
use App\Models\Track;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    protected $rules = [
      'term' => 'required'
    ];

    public function index(Request $request) {
        $term = $request->input('term');
        return new SearchResource(Auth::user()->id);
    }
}
