<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TrackResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $contributors = [];
        foreach ($this->contributing_artists as $contrib) {
            $contributors[] = [
                'id' => $contrib->artist->id,
                'link' => '/artist/' . $this->artist->id,
                'name' => $contrib->artist->name
            ];
        }
        return [
            'id' => $this->id,
            'title' => $this->title,
            'trackLength' => $this->length / 1000,
            'trackNumber' => $this->track_number,
            'src' => '/files/tracks/' . $this->id . '.mp3',
            'artistId' => $this->artist_id,
            'artistName' => $this->artist->name,
            'artistLink' => '/artist/' . $this->artist->id,
            'albumId' => $this->album_id,
            'albumCover' => '/files/artwork/' . $this->album->id . '.jpg',
            'albumTitle' => $this->album->title,
            'albumYear' => $this->album->year,
            'albumLink' => '/album/' . $this->album->id,
            'contributors' => $contributors,
            'links' => [
                'album' => '/album/' . $this->album->id,
                'artist' => '/artist/' . $this->artist->id,
                'track' => '/files/tracks/' . $this->id . '.mp3'
            ]
        ];
    }
}
