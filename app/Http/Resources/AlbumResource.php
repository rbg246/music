<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class AlbumResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'albumTitle' => $this->title,
            'albumYear' => $this->year,
            'artistName' => $this->artist->name,
            'artistId' => $this->artist->id,
            'links'=> [
                'albumCover' => '/files/artwork/' . $this->id . '.jpg',
                'album' => '/album/' . $this->id,
                'artist' => 'artist/' . $this->artist->id
            ],
            'tracks' => TrackResource::collection($this->tracks)
        ];
    }
}
