<?php

namespace App\Http\Resources\Playlists;

use App\Http\Resources\TrackResource;
use Illuminate\Http\Resources\Json\Resource;

class PlaylistItemResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $trackResource = new TrackResource($this->track);
        return $trackResource;
    }
}
