<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ContributingArtistResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $track = new TrackResource($this->track);
        return $track;
    }
}
