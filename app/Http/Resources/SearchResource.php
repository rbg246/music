<?php

namespace App\Http\Resources;

use App\Models\Album;
use App\Models\Artist;
use App\Models\Track;
use Illuminate\Http\Resources\Json\Resource;

class SearchResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $term = $request->input('term');

        $tracks = Track::search($term)->get();
        $albums = Album::search($term)->get();
        $artists = Artist::search($term)->get();

        return [
            'tracks' => TrackResource::collection($tracks),
            'albums' => AlbumResource::collection($albums),
            'artists' => ArtistResource::collection($artists)
        ];
    }
}
