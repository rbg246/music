<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\PlaylistResource;

class UserResource extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'userData' => json_decode($this->play_queue),
            'playlists' => PlaylistResource::collection($this->playlists),
            'roles' => $this->roles->toArray()
        ];
    }


    public function update ($request) {
        $queue = $request->input('user');
        $user = Auth::user();

        if ($queue !== NULL
          && is_array($queue)
          &&  isset($queue['currentTrack'])
          && isset($queue['playQueue'])) {
            $user->play_queue = json_encode($queue);
            $user->save();
            return response()->json([
              'user' => $queue
            ]);
        } else {
            return response()->json([
              'error' => TRUE,
              'message' => 'queue was not valid JSON or NULL',
            ], 400);
        }
    }
}
