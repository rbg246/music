<?php

namespace App\Http\Resources;

use App\Models\Track\ContributingArtist;
use Illuminate\Http\Resources\Json\Resource;

class ArtistResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $contributed_on = ContributingArtist::where('artist_id', '=', $this->id)
            ->with('artist', 'track')->get();
        $collection = ContributingArtistResource::collection($contributed_on);
        return [
          'id' => $this->id,
          'artistName' => $this->name,
          'albums' => AlbumResource::collection($this->albums),
          'contributedOn' => $collection,
          'links' => [
            'artist' => '/artist/' . $this->id
          ]
        ];
    }
}
