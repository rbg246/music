<?php

namespace App\Http\Resources;

use App\Http\Resources\Playlists\PlaylistItemResource;
use Illuminate\Http\Resources\Json\Resource;

class PlaylistResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'created_at' => $this->created_at,
            'tracks' => PlaylistItemResource::collection($this->tracks)
        ];
    }
}
