<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $auth= Auth::user();
        $uid = $this->route('user');
        $ret =  ($auth->id === $uid || $auth->hasRole('admin'));
        return $ret;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name' => 'required|String',
//          'email' =>  Rule::unique('users', 'email')->ignore($user->id),
          'password' => 'confirmed',
          'roles' => 'required'
        ];
    }

    public function messages () {
        return [
            'name.required' => 'User\'s name is required',
            'password.confirmed' => 'Passwords do not match',
            'roles.required' => 'At least one role is required'
        ];
    }
}
