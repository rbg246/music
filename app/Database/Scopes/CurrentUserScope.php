<?php declare(strict_types=1);


namespace App\Database\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class CurrentUserScope implements Scope
{

    /**
     * Scope a query to only include playlists of a user
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function apply(Builder $builder, Model $model)
    {
        $user = Auth::user();
        $builder->where('user_id', '=', $user->id);
    }
}