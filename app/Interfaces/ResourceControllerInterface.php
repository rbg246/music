<?php declare(strict_types=1);

namespace App\Interfaces;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

interface ResourceControllerInterface {

    /**
     * Destroy a resource.
     * @param string|int      $primaryKey
     * @param null|string|int $parentKey
     *
     * @return View
     */
    public function destroy($primaryKey, $parentKey = null): View;

    /**
     * Get paginated list of resources.
     * @param null|string|int $parentKey
     *
     * @return View
     */
    public function index($parentKey = null): View;

    /**
     * Get a single resource.
     * @param string|int      $primaryKey
     * @param null|string|int $parentKey
     *
     * @return View
     */
    public function show($primaryKey, $parentKey = null): View;

    /**
     * return view for creating a resource
     * @return View
     */
    public function create(): View;


    /**
     * Create and store a single resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param null|string|int $parentKey
     *
     * @return View
     */
    public function store(Request $request, $parentKey = null): View;

    /**
     * return a view for updating a resource
     * @param $primaryKey
     *
     * @return \Illuminate\View\View
     */
    public function edit ($primaryKey) : View;

    /**
     * Update a single resource.
     *
     * @param Request $request
     * @param string|int $primaryKey
     * @param null|string|int $parentKey
     *
     * @return View
     */
    public function update(Request $request,  $primaryKey, $parentKey = null): View;

}