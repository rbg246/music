<?php declare(strict_types=1);

namespace App\Interfaces;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

interface ApiControllerInterface {

    /**
     * Destroy a resource.
     * @param string|int      $primaryKey
     * @param null|string|int $parentKey
     *
     * @return Resource
     */
    public function destroy($primaryKey, $parentKey = null): Resource;

    /**
     * Get paginated list of resources.
     * @param null|string|int $parentKey
     *
     * @return Resource
     */
    public function index($parentKey = null): Resource;

    /**
     * Get a single resource.
     * @param string|int      $primaryKey
     * @param null|string|int $parentKey
     *
     * @return Resource
     */
    public function show($primaryKey, $parentKey = null): Resource;

    /**
     * Create and store a single resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param null|string|int $parentKey
     *
     * @return Resource
     */
    public function store(Request $request, $parentKey = null): Resource;

    /**
     * Update a single resource.
     *
     * @param Request $request
     * @param string|int $primaryKey
     * @param null|string|int $parentKey
     *
     * @return Resource
     */
    public function update(Request $request,  $primaryKey, $parentKey = null): Resource;

}