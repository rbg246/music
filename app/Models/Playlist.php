<?php

namespace App\Models;

use App\Database\Scopes\CurrentUserScope;
use Illuminate\Database\Eloquent\Model;
use App\Models\Playlist\PlaylistItem;
use Mockery\Exception;
use Illuminate\Support\Facades\Auth;

class Playlist extends Model
{

    /**
     * references database table
     * @var string
     */
    protected $table = 'playlists';

    /**
     * fillable fields
     * @var array
     */
    protected $fillable = [
      'title',
      'user_id'
    ];

    /**
     * hidden fields
     * @var array
     */
    protected $hidden = [
      'updated_at'
    ];

    /**
     * booting method of the Model
     */
    protected static function boot ()
    {
        parent::boot();

        static::addGlobalScope(new CurrentUserScope);
    }

    /**
   * adds a track to the playlist
   * @param $track_id
   *
   * @return \App\Models\Playlist\PlaylistItem|null
   */
    public function add_track($track_id) {
      if ($track_id !== NULL) {
        $track = Track::find($track_id);
        if ($track)
        try {
          $track = Track::findOrFail($track_id);
        } catch (Exception $e) {
          return NULL;
        }
        $playlist_item = new PlaylistItem;
        $playlist_item->track_id = $track->id;
        $playlist_item->playlist_id = $this->id;
        $playlist_item->save();
        return $playlist_item;
      }
      return NULL;
    }

  /**
   * creates a One to Many relationship with playlist items
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
    public function tracks () {
      return $this->hasMany('App\Models\Playlist\PlaylistItem',
        'playlist_id', 'id');
    }
}
