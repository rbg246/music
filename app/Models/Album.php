<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * @property mixed album_cover
 * @property mixed year
 * @property mixed title
 * @property mixed artist_id
 * £
 *
 */
class Album extends Model
{

    use SearchableTrait;

    const FILE_ART = 'album_artwork';

    protected $fillable = [
        'title',
        'artist_id',
        'year',
        'album_cover'
    ];

    protected $searchable = [
      'columns' => [
        'albums.title' => 10,
        'albums.year' => 3
      ]
    ];

    public function artist() {
      return $this->hasOne('App\Models\Artist', 'id','artist_id');
    }

    public function tracks() {
        return $this->hasMany('App\Models\Track',
          'album_id', 'id')
          ->orderBy('track_number', 'asc');
    }

  /**
   * get all albums from database
   * @return mixed
   */
    static public function all_albums () {
      $albums = Album::get();
      return $albums;
    }

  /**
   * get album artwork url
   * @return string
   */
    public function get_artwork_url() {
      $url = ($this->album_cover !== NULL) ?
        '/files/artwork/' . $this->id .'.jpg' :
        '/images/music.png';
      return $url;
    }

  /**
   * gets url for editing album
   * @return string
   */
    public function get_album_edit_url() {
      $url = '/admin/albums/album/' . $this->id .  '/edit';
      return $url;
    }

  /**
   * gets url for viewing album
   * @return string
   */
    public function get_album_view_url() {
      $url = '/music/' . $this->id;
      return $url;
    }

  /**
   *
   * @param \Illuminate\Http\UploadedFile $file
   */
    public function store_artwork($file) {
      $file_path = self::FILE_ART;
      $file_name = $this->id;
      $ext = $file->clientExtension();
      $file_name = (!$ext) ? $file_name : $file_name . '.' . $ext;

      try {
        $file->storeAs($file_path, $file_name);
        $this->album_cover = implode('/', [$file_path, $file_name]);
      } catch (Exception $e) {
        Log::error('Unable to save file', $this->track);
      }

    }
}
