<?php

namespace App\Models\Playlist;

use Illuminate\Database\Eloquent\Model;

class PlaylistItem extends Model
{

    /**
     * @var string
     */
    protected $table = 'playlist_items';

    /**
     * sets the mass-assignable properties
     * @var array
     */
    protected $fillable = [
      'track_id',
      'playlist_id'
    ];

    /**
     * gets the associated track associatd with this track_id
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function track () {
      return $this->hasOne('App\Models\Track', 'id', 'track_id');
    }

}
