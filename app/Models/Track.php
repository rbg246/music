<?php

namespace App\Models;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use getID3;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Nicolaslopezj\Searchable\SearchableTrait;
use App\Models\Track\ContributingArtist;


class Track extends Model
{
    use SearchableTrait;
    /**
     * unprocessed mp3 tags
     * @var array $tags
     */
    private $tags;

    /**
     * uploaded file from request
     * @var UploadedFile $file
     */
    private $file;

    /**
     * mass assignable properties
     * @var array
     */
    protected $fillable = [
        'title',
        'artist_id',
        'album_id',
        'file_path',
        'length',
        'track_number',
        'genre',
        'processed'
    ];

    protected $searchable = [
        'columns' => [
            'tracks.title' => 10,
        ],
    ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'processed' => 'boolean',
  ];

    // file types
    const FILE_MUSIC = 'music';
    const FILE_UNPROCESSED = 'unprocessed';

    // default values

    const DEFAULT_ID = 1;
    const DEFAULT_ALBUM = 'Default Album';
    const DEFAULT_ARTIST = 'Default Artist';
    const DEFAULT_TRACK = 'Default Title';

    private $contributing = [];
    /**
     * gets the associated album with this track
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function album () {
        return $this->hasOne('App\Models\Album', 'id', 'album_id');
    }

  /**
   * gets the associated ArtistResource for the track
   * @return \Illuminate\Database\Eloquent\Relations\HasOne
   */
    public function artist () {
      return $this->hasOne('App\Models\Artist', 'id', 'artist_id');
    }

    /**
     * gets the contributing artists
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contributing_artists () {
        return $this->hasMany('App\Models\Track\ContributingArtist',
          'track_id', 'id');
    }


  /**
   * creates a track, and if need be, album and artist
   *
   * @param array $attrs
   *
   * @return \App\Models\Track|null
   */
    static public function create ($attrs) {
        // creates a validation error to return to user for appropriate

        $track = new Track($attrs);
        $track->processed = 1;
        $track->file = $attrs['file'];
        $track->extract_id3_tags();
        $track->process_tags();
        $track->create_artist_album();
        $pre_existing = Track::where('title', $track->title)
          ->where('album_id', $track->album_id)
          ->count();

        $success = ($pre_existing > 0) ? false : $track->store_track();
        if ($success) {
            $track->save();
            $track->attachContributingArtists();
        } else {
          // set message saying save unsuccessful
          return NULL;
        }

        return $track;
    }

  /**
   *  fills the fillable properties
   */
    private function process_tags () {
      // text fields that need
      // album and artist are required at create album etc
      $required_tags = [
        'title',
        'length',
        'track_number',
        'genre'
      ];
      // processed until we miss some info
      $this->processed = 1;

      foreach ($this->fillable as $field) {
        $field_value = $this->get_tag($field);
        if ($field_value !== NULL) {
          $this->{$field} = $field_value;
        } else if (in_array($field, $required_tags)) {
          $this->processed = 0;
        }
      }
    }
  /**
   * gets tag that has been extracted
   * @param string $tag
   *
   * @return string | null
   */
    private function get_tag ($tag) {
      return (isset($this->tags[$tag])) ? $this->tags[$tag] : NULL;
    }

    /**
     * extracts the files id3 tags
     */
    private function extract_id3_tags () {
        // Initialize getID3 engine
        $getID3 = new getID3;
        $file_info = $getID3->analyze($this->file->path());
        $tags = $file_info['tags']['id3v2'];
        foreach ($tags as $tag_name => $tag) {
          $tags[$tag_name] = implode(', ', $tag);
        }
        $this->tags = $tags;
    }

  /**
   * generates filepath for tracks and album art to be saved
   *
   * @param string $file_type
   *
   * @return array
   * @internal param \Illuminate\Http\UploadedFile $file
   * @internal param string $type
   */
    public function build_file_path ($file_type = self::FILE_MUSIC) {
        $file_info = [
            'file_path' => '',
            'file_name' => ''
        ];

        switch ($file_type) {
            case self::FILE_UNPROCESSED:
                $ext = (isset($this->file)) ? $this->file->clientExtension()
                :  Storage::mimeType($this->file_path);
                $ext = ($ext === NULL) ? 'mp3' : $ext;
                $file_info['file_path'] = $file_type . '/';
                $file_info['file_name'] = $this->file->hashName() . '.' . $ext;
                break;
            case self::FILE_MUSIC:
                $artist_name = str_replace(' ', '-', $this->artist->name);
                $album_name = str_replace(' ', '-', $this->album->title);
                $track_name = str_replace(' ', '-', $this->title);
                $ext = (isset($this->file)) ? $this->file->clientExtension()
                  :  Storage::mimeType($this->file_path);
                $ext = ($ext === NULL || 'audio/mpeg') ? 'mp3' : $ext;
                $file_path = $artist_name . '/' . $album_name;
                $file_info['file_path'] = $file_type . '/' . $file_path;
                $file_name = $track_name . '.' . $ext;
                $file_info['file_name'] = $file_name;
                break;
        }

        // append base directory to file path
        return $file_info;
    }

    /**
     * handles saving of uploaded file either as unprocessed file or processed
     * @return bool
     */
    private function store_track() {
        if (isset($this->file)) {
            $file_type = $file_path = (!$this->processed)
                ? self::FILE_UNPROCESSED : self::FILE_MUSIC;
            $file_info = $this->build_file_path($file_type);

            try {
                $this->file->storeAs($file_info['file_path'], $file_info['file_name']);
                // remove old file
                if ($this->file_path !== NULL) {
                  Storage::delete($this->file_path);
                }
                $this->file_path = implode('/', $file_info);
            } catch (Exception $e) {
              Log::error('Unable to save file');
              return FALSE;
            }
        }
        return TRUE;
    }

  /**
   * create artist and album
   */
    private function create_artist_album() {

      // create artist
      if ($this->get_tag('artist') !== NULL) {

        $artist_names = explode(';', $this->get_tag('artist'));
        $primary_artist = trim(array_shift($artist_names));
        $artist = Artist::firstOrCreate([
          'name' => $primary_artist
        ]);
          // create contributing artist model and we then
          // save the contributing artist to an array
          // we connect with track_id once we have saved model
        foreach ($artist_names as $contributing_artist) {
            $contrib = Artist::firstOrCreate([
              'name' => trim($contributing_artist)
            ]);
            $this->contributing[] = $contrib->id;
        }

        // set the contributing artists until we have saved the model
          // and have id

        $this->artist_id = $artist->id;
      } else {
        $this->artist_id = self::DEFAULT_ID;
        $this->processed = 0;
      }

      if ($this->get_tag('album') !== NULL) {
        $year = ($this->get_tag('year') === NULL) ? '' :
          $this->get_tag('year');
        $album = Album::firstOrCreate([
          'title' => $this->get_tag('album'),
          'artist_id' => $this->artist_id,
          'year' => $year
        ]);
        $this->album_id = $album->id;
      } else {
        $this->album_id = self::DEFAULT_ID;
        $this->processed = 0;
      }
    }

  /**
   * return all tracks less default track
   * @return mixed
   */
    static function all_tracks() {
      $tracks = DB::table('tracks')
        ->join('albums', 'tracks.album_id', '=', 'albums.id')
        ->join('artists', 'tracks.artist_id', '=', 'artists.id')
        ->select(
          'tracks.id as id',
          'artists.name as artist',
          'albums.title as album',
          'tracks.track_number as track_number',
          'tracks.title as track',
          'tracks.length as length'
        )
        ->where('tracks.processed', '=', 1)
        ->orderBy('artists.name', 'albums.title', 'tracks.track_number')
        ->get()
        ->toArray();

      // formatting before returning
      foreach ($tracks as &$track) {
        $seconds = round($track->length / 1000);
        $track->length = date('i:s', $seconds);
      }

      return $tracks;
    }

    static public function unprocessed_tracks () {
      $tracks = DB::table('tracks')
        ->join('albums', 'tracks.album_id', '=', 'albums.id')
        ->join('artists', 'tracks.artist_id', '=', 'artists.id')
        ->select(
          'tracks.id as id',
          'artists.name as artist',
          'albums.title as album',
          'tracks.track_number as track_number',
          'tracks.title as track',
          'tracks.length as length'
        )
        ->where('tracks.processed', '=', 0)
        ->orderBy('artists.name', 'albums.title', 'tracks.track_number')
        ->get();
      return $tracks;
    }

  /**
   * processes unprocessed tracks from form
   * @param array $tracks
   */
    public static function process_unprocessed_tracks ($tracks) {

      foreach ($tracks as $track_id => $track_data) {
        $track = Track::find($track_id);

        if (isset($track_data['album_title'])) {
          $album = Album::firstOrCreate([
            'title' => $track_data['album_title']
          ]);
          $track->album_id = $album->id;
        }

        if (isset($track_data['artist_name'])) {
          $artist = Artist::firstOrCreate([
            'name' => $track_data['artist_name']
          ]);
          $track->artist_id = $artist->id;
        }

        if (isset($track_data['track_number'])) {
          $track->track_number = $track_data['track_number'];
        }

        if (isset($track_data['track_title'])) {
          $track->title = $track_data['track_title'];
        }
        $processed = $track->processed;
        if (!$processed) {
          $track->processed = (int) self::is_processed($track);
          if ($track->processed) {
            $file_path = $track->file_path;
            $new_file_path = $track->build_file_path(self::FILE_MUSIC);
            try {
              $new_file_path = implode('/', $new_file_path);
              Storage::move($file_path, $new_file_path);
              $track->file_path = $new_file_path;
            } catch (Exception $e) {
              Log::error('Unable to save file to S3 (first attempt)');
            }
          }
        }
        $track->save();
      }
    }

  /**
   * @param \App\Models\Track $track
   *
   * @return bool
   */
    public static function is_processed (Track $track) {
      $processed = TRUE;
      if ($track->title === self::DEFAULT_TRACK) {
        $processed = FALSE;
      }

      if ($track->album_id === self::DEFAULT_ID) {
        $processed = FALSE;
      }

      if ($track->artist_id === self::DEFAULT_ID) {
        $processed = FALSE;
      }

      return $processed;
    }

  /**
   * get file url for track
   * @return string
   */
    public function get_track_url() {
      $url = '/files/track/' . $this->id . '.mp3';
      return $url;
    }

  /**
   * returns url for editing a track
   * @return string
   */
    public function get_edit_url() {
      $url = '/admin/tracks/track/' . $this->id . '/edit';
      return $url;
    }

  /**
   * @param \Illuminate\Http\UploadedFile $file
   */
    public function replace_track (UploadedFile $file) {
      if ($this->file_path !== NULL) {
        try {
          Storage::delete($this->file_path);
          $this->file_path = NULL;
          $this->save();
        } catch (Exception $exception) {
          Storage::error('Unable to delete file', $this->file_path);
        }
      }
      $file_info = $this->build_file_path(self::FILE_MUSIC);
      $file_path = $file_info['file_path'];
      $file_name = $file_info['file_name'];
      try {
        $file->storeAs($file_path, $file_name);
      } catch (Exception $exception) {
        Storage::error('Unable to store file', implode('/', $file_info));
      }
    }

    /**
     * attaches contributing artists to Track
     */
    public function attachContributingArtists () {
        foreach ($this->contributing as $artist_id) {
            $attrs = [
                'track_id' => $this->id,
                'artist_id' => $artist_id
            ];
            $contributing = new ContributingArtist;
            $contributing->fill($attrs);
            $contributing->save();
        }
    }
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }
}
