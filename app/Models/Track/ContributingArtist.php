<?php

namespace App\Models\Track;

use Illuminate\Database\Eloquent\Model;

class ContributingArtist extends Model
{

    protected $table = 'contributing_artists';

    protected $fillable = [
        'track_id',
        'artist_id'
    ];

    public function artist() {
        return $this->hasOne('App\Models\Artist', 'id', 'artist_id');
    }

    public function track() {
        return $this->hasOne('App\Models\Track', 'id', 'track_id');
    }
}
