<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Artist extends Model
{

    use SearchableTrait;

    protected $fillable = [
        'name'
    ];

    protected $searchable = [
      'columns' => [
        'artists.name' => 10
      ]
    ];

  /**
   * returns url for editing artists
   */
    public function get_artist_edit_url () {
      $url = '/admin/artists/artist/' . $this->id . '/edit';
      return $url;
    }


    public function albums () {
        return $this->hasMany('App\Models\Album',
          'artist_id', 'id');
    }
}
