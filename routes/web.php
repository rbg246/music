<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Artist;
use App\Models\Album;
use App\Models\Playlist;
use App\Models\Track;
use Illuminate\Support\Facades\Route;
use App\Http\Resources\UserResource;
use App\Http\Resources\ArtistResource;
use App\Http\Resources\AlbumResource;
use App\Http\Resources\AlbumSummaryResource;
use App\Http\Resources\TrackResource;
use App\Http\Resources\PlaylistResource;


Route::get('/logout', 'Auth\LoginController@logout');

/**
 * App routes
 */


Route::get('/', 'MusicController@home');

Route::prefix('app')->group(function() {
  Route::get('{any?}/{route?}', 'MusicController@home');
  Route::get('/playlist/{playlist}/edit', 'MusicController@home');
});


/**
 * File serving routes
 */
Route::get('/files/tracks/{track}.mp3', 'FileController@stream_track');
Route::get('/files/artwork/{album}.jpg', 'FileController@serve_artwork');


/**
 * uploading and editing music routes
 */
Route::prefix('admin')->middleware('role:admin|uploader')->group(function () {

  Route::get('/', 'MusicController@index')->name('admin');

  Route::get('/upload', 'UploadController@index')->name('upload');
  Route::post('/upload', 'UploadController@upload');

  Route::get('/albums', 'MusicController@albums');
  Route::get('/albums/edit', 'MusicController@editAlbumList')->name('editAlbums');
  Route::get('/albums/album/{album}/edit', 'MusicController@editAlbum');
  Route::post('/albums/album/{album}/edit', 'MusicController@editAlbumSubmit');

  Route::get('/tracks', 'MusicController@tracks');
  Route::get('/tracks/edit', 'MusicController@editTrackList')->name('editTracks');
  Route::get('/tracks/track/{track}/edit', 'MusicController@editTrack');
  Route::post('/tracks/track/{track}/edit', 'MusicController@editTrackSubmit');

  Route::get('/artists', 'MusicController@artists');
  Route::get('/artists/edit', 'MusicController@editArtistList')->name('editArtists');
  Route::get('/artists/artist/{artist}/edit', 'MusicController@editArtist');
  Route::post('/artists/artist/{artist}/edit', 'MusicController@editArtistSubmit');

  Route::get('/unprocessed-tracks', 'MusicController@unprocessed_tracks')->name('unprocessedTracks');
  Route::post('/unprocessed-tracks', 'MusicController@process_tracks');


});


/**
 * routes for editing users
 */
Route::prefix('admin')->middleware('role:admin')->group(function () {

    Route::resource('user', 'UserController');

});

/**
 * api routes
 */
Route::prefix('api/v1')->group(function () {

  Route::get('user', function () {
      return new UserResource(Auth::user());
  });
  Route::post('user', 'APIController@update_user');

  Route::post('search', 'SearchController@index');
  // get single
  Route::get('artist/{artist}', function (Artist $artist) {
      return new ArtistResource($artist);
  });
  Route::get('album/{album}', function (Album $album) {
      return new AlbumResource($album);
  });
  Route::get('track/{track}', function (Track $track) {
      return new TrackResource($track);
  });

  // get paginated results
  Route::get('artists/{page?}', function ($page) {
      $artists = Artist::where('id', '!=', 1)
        ->simplePaginate(50, [] , '', $page);
      return ArtistResource::collection($artists);
  });

  Route::get('albums/{page?}', function () {
      $albums = Album::where('id', '!=', '1')
        ->simplePaginate(25, ['*'] , '');
      return AlbumSummaryResource::collection($albums);
  });

  Route::get('tracks/{page?}', function () {

  });

  /**
   * playlist api
   */

  Route::resource('playlist', 'PlaylistController', [
    'except' => ['create', 'edit']
  ]);
  Route::resource('playlist/{playlist}/item', 'PlaylistItemController', [
    'except' => ['index', 'create', 'update', 'show']
  ]);

});

Auth::routes();
